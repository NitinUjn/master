import axios from 'axios';
const API_URL = 'https://gm-hiring.myres.cc/api/v1/assessments/d558f91e-4f58-4599-a052-c37de39b27b8';
export class APIService{
    
constructor(){
}

getProducts() {
    const url = `${API_URL}/products`;
    return axios.get(url).then(response => response.data);
}

getProduct(id) {
    const url = `${API_URL}/products/${id}`;
    return axios.get(url).then(response => response.data);
}

createContact(contact){

    const url = `${API_URL}/products/`;
    return axios.post(url,contact);
}

updateProduct(contact){
    const url = `${API_URL}/products/${contact.pk}`;
    return axios.put(url,contact);
}

deleteContact(productId){
    const url = `${API_URL}/products/${productId}`;
    return axios.delete(url);
}

}